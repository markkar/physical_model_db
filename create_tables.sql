CREATE TABLE models
(
    id    SERIAL PRIMARY KEY NOT NULL,
    name  VARCHAR(50)        NOT NULL,
    brand VARCHAR(50)        NOT NULL
);

CREATE TABLE clients
(
    id            SERIAL PRIMARY KEY NOT NULL,
    first_name    CHARACTER(10)      NOT NULL,
    second_name   CHARACTER(20)      NOT NULL,
    patronomic    CHARACTER(20)      NOT NULL,
    country       CHARACTER(10),
    rating        SMALLINT,
    sex           CHARACTER(1)       NOT NULL,
    date_of_birth DATE               NOT NULL,
    email         CHARACTER(20),
    pass_num      CHARACTER(12)      NOT NULL,
    driver_num    CHARACTER(12)      NOT NULL
);

CREATE TABLE order_demands
(
    id               SERIAL PRIMARY KEY NOT NULL,
    gps              BOOLEAN            NOT NULL,
    wifi             BOOLEAN            NOT NULL,
    own_driver       BOOLEAN            NOT NULL,
    baby_chair       BOOLEAN            NOT NULL,
    air_conditioning BOOLEAN            NOT NULL
);

CREATE TABLE pickup_places
(
    id      SERIAL PRIMARY KEY NOT NULL,
    country VARCHAR(30)        NOT NULL,
    city    VARCHAR(40)        NOT NULL,
    street  VARCHAR(30)        NOT NULL,
    number  SMALLINT           NOT NULL
);

CREATE TABLE client_orders
(
    id             SERIAL PRIMARY KEY NOT NULL,
    demandsID      INTEGER DEFAULT 1,
    clientID       INTEGER,
    pickup_placeID INTEGER,
    get_date       DATE               NOT NULL,
    return_date    DATE               NOT NULL,
    comment        TEXT,
    order_time     TIMESTAMP          NOT NULL,

    FOREIGN KEY (demandsID) REFERENCES order_demands (id) ON DELETE NO ACTION,
    FOREIGN KEY (clientID) REFERENCES clients (id) ON DELETE SET NULL,
    FOREIGN KEY (pickup_placeID) REFERENCES pickup_places (id) ON DELETE SET NULL
);

CREATE TABLE contacts
(
    id       SERIAL PRIMARY KEY NOT NULL,
    clientID INTEGER,
    type     VARCHAR(20)        NOT NULL,
    value    VARCHAR(40)        NOT NULL,

    FOREIGN KEY (clientID) REFERENCES clients (id) ON DELETE CASCADE
);

CREATE TYPE TRANSMISSION AS ENUM ('auto', 'semiauto', 'manual');
CREATE TYPE DRIVE_TYPE AS ENUM ('front', 'back', 'full');
CREATE TYPE ENGINE_TYPE AS ENUM ('petrol', 'electric', 'diesel');

CREATE TABLE cars
(
    number               VARCHAR(15) PRIMARY KEY          NOT NULL,
    modelID              INTEGER                          NOT NULL,
    orderID              INTEGER,
    placeID              INTEGER                          NOT NULL,
    photo                VARCHAR(50),
    car_type             VARCHAR(30)                      NOT NULL,
    cost_per_day         DECIMAL(10, 2)                   NOT NULL,
    max_passenger_number SMALLINT                         NOT NULL,
    last_repair_date     DATE,
    engine_power         DECIMAL(7, 2)                    NOT NULL,
    air_conditioner      BOOLEAN                          NOT NULL,
    transmission         TRANSMISSION                     NOT NULL,
    fuel_cons            DECIMAL(5, 2)                    NOT NULL,
    prod_year            SMALLINT                         NOT NULL,
    drive_type           DRIVE_TYPE                       NOT NULL,
    mileage              INTEGER,
    engine_type          ENGINE_TYPE                      NOT NULL,
    trunk_volume         SMALLINT,
    color                VARCHAR(20)                      NOT NULL,
    steering_side        VARCHAR(1)                       NOT NULL,
    min_driver_age       SMALLINT                         NOT NULL,
    rating               DECIMAL(2, 1) CHECK (rating > 0) NOT NULL,

    FOREIGN KEY (modelID) REFERENCES models (id) ON DELETE NO ACTION,
    FOREIGN KEY (orderID) REFERENCES client_orders (id) ON DELETE SET NULL,
    FOREIGN KEY (placeID) REFERENCES pickup_places (id) ON DELETE CASCADE
);